package com.example.diabete.diabeteappli.items;

import java.util.List;

/**
 * Created by mathilde on 06/04/2017.
 */

public class Proposition {
    private String usedProtocol;
    private boolean needsSugar;
    private List<Product> products;

    public void Proposition (String usedProtocol, boolean needsSugar) {
        this.usedProtocol = usedProtocol;
        this.needsSugar = needsSugar;
    }

    public void Proposition (String usedProtocol, boolean needsSugar, List<Product> products) {
        this.usedProtocol = usedProtocol;
        this.needsSugar = needsSugar;
        this.products = products;
    }

    public String getUsedProtocol() {
        return this.usedProtocol;
    }

    public void setUsedProtocol(String usedProtocol) {
        this.usedProtocol = usedProtocol;
    }

    public boolean isNeedsSugar() {
        return this.needsSugar;
    }

    public void setNeedsSugar(boolean needsSugar) {
        this.needsSugar = needsSugar;
    }

    public List<Product> getProducts() {
        return this.products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
