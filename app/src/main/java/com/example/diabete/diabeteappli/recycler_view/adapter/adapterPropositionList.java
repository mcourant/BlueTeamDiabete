package com.example.diabete.diabeteappli.recycler_view.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.diabete.diabeteappli.R;
import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.mealManager.MealManager;
import com.example.diabete.diabeteappli.recycler_view.viewholder.viewHolderMealProposition;
import com.example.diabete.diabeteappli.recycler_view.viewholder.viewHolderPropositionList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.diabete.diabeteappli.items.Product.getProductsFromDB;

/**
 * Created by mcourant on 31/03/2017.
 */

public class adapterPropositionList extends RecyclerView.Adapter<viewHolderPropositionList> {

    //private List<String> list_proposition = new ArrayList<>();
    private Map<String, float[]> meal = new HashMap<>();
    private List<String[]> propositionDetail = new ArrayList<>();
    private List<Product> listProduct = new ArrayList<>();
    private List<Product> listSelected = new ArrayList<>();

    /*public adapterPropositionList(List<String> list_proposition) {
        this.list_proposition = list_proposition;
    }*/

    public adapterPropositionList(Map<String, float[]> meal, String business, Context context) {
        this.meal = meal;

        for (String productName : meal.keySet()) {
            String[] product = new String[2];
            product[0] = productName;
            product[1] = Float.toString(meal.get(productName)[3]);

            this.propositionDetail.add(product);
        }

        listProduct = getProductsFromDB(context, business, "");


        for(int j = 0; j< propositionDetail.size(); j++){
            for(int i = 0;i<listProduct.size();i++){
                String z = propositionDetail.get(j)[0].split("_")[0];
                if(z.equals(listProduct.get(i).getName())){
                    listSelected.add(listProduct.get(i));
                }
            }
        }

    }

    @Override
    public viewHolderPropositionList onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewProduct = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_meal_proposition,parent,false);
        return new viewHolderPropositionList(viewProduct);
    }

    @Override
    public void onBindViewHolder(viewHolderPropositionList holder, int position) {
        float t = Float.valueOf(this.propositionDetail.get(position)[1]);
        int e = (int) t;

        holder.getImageView().setImageResource(listSelected.get(position).getImage());

        holder.getTextviewFirst().setText(this.propositionDetail.get(position)[0].split("_")[0]);
        holder.getTextviewSecond().setText(e+"%");

    }

    @Override
    public int getItemCount() {
        return this.propositionDetail.size();
    }
}
