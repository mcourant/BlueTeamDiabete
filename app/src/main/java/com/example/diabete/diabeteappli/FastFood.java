package com.example.diabete.diabeteappli;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diabete.diabeteappli.items.Product;

import java.util.List;

public class FastFood extends AppCompatActivity {
    public static final String BUSINESS = "business";
    public static final String MCDO = "McDo";
    public static final String QUICK = "Quick";
    public static final String MEAL = "meal";

    public String business = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        checkSettings(); // check if protocols are set

        setContentView(R.layout.activity_fast_food);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ImageView imageMcdo = (ImageView) findViewById(R.id.macdoImage);
        ImageView imageQuick = (ImageView) findViewById(R.id.quickImage);

        imageMcdo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                business = MCDO;
                displayMealPopup();

            }
        });

        imageQuick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                business = QUICK;
                displayMealPopup();
            }
        });

    }

    public void displayMealPopup() {
        LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup activityView = (ViewGroup)((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);

        final PopupWindow mealChoicePopup = new PopupWindow(inflater.inflate(R.layout.popup_meal_choice, null),
                (int)Math.round(activityView.getWidth()/2*1.6),
                (int)Math.round(activityView.getHeight()/2*1.6),
                true);
        mealChoicePopup.setBackgroundDrawable(new BitmapDrawable()); //dismiss the popup on outside click
        mealChoicePopup.showAtLocation(new RelativeLayout(this), Gravity.CENTER, 0, 0);

        final View popupView = mealChoicePopup.getContentView();

        // OK button
        popupView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectedButton = ((RadioGroup)popupView.findViewById(R.id.radioMeal)).getCheckedRadioButtonId();
                int selectedMeal = Integer.valueOf(popupView.findViewById(selectedButton).getTag().toString());

                Intent intent = new Intent(FastFood.this, MainActivity.class);
                intent.putExtra(BUSINESS, business);
                intent.putExtra(MEAL, selectedMeal);
                startActivity(intent);
            }
        });

        // Cancel button
        popupView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mealChoicePopup.dismiss();
            }
        });
    }

    private void checkSettings() { // if protocols not set, redirects to the settings page
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        float breakfastFast = preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_FAST, 0f);
        float breakfastSlow = preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_SLOW, 0f);
        float lunchFast = preferences.getFloat(ProtocolSettingsActivity.LUNCH_FAST, 0f);
        float lunchSlow = preferences.getFloat(ProtocolSettingsActivity.LUNCH_SLOW, 0f);
        float snackFast = preferences.getFloat(ProtocolSettingsActivity.SNACK_FAST, 0f);
        float snackSlow = preferences.getFloat(ProtocolSettingsActivity.SNACK_SLOW, 0f);
        float dinnerFast = preferences.getFloat(ProtocolSettingsActivity.DINNER_FAST, 0f);
        float dinnerSlow = preferences.getFloat(ProtocolSettingsActivity.DINNER_SLOW, 0f);

        if (breakfastFast == 0 || breakfastSlow == 0 || lunchFast == 0 || lunchSlow == 0
                || snackFast == 0 || snackSlow == 0 || dinnerFast == 0 || dinnerSlow == 0) {
            Intent settingsIntent = new Intent(this, ProtocolSettingsActivity.class);
            startActivity(settingsIntent);
        }
    }

}
