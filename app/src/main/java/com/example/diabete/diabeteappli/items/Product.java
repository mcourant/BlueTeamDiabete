package com.example.diabete.diabeteappli.items;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.diabete.diabeteappli.database.BaseContrat;
import com.example.diabete.diabeteappli.database.DataBaseHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by mcourant on 31/03/2017.
 */

public class Product {

    private String business;
    private String category;
    private String name;
    private float carbohydrates;
    private float fast_sugar;
    private int image;
    private int quantity;

    public Product(String business, String category, String name, float carbohydrates, float fast_sugar, int image){
        this.business = business;
        this.category = category;
        this.name = name;
        this.carbohydrates = carbohydrates;
        this.fast_sugar = fast_sugar;
        this.image = image;
    }

    private static Product getProductFromCursor(Cursor cursor) {
        return new Product(cursor.getString(cursor.getColumnIndex(BaseContrat.ProductsContrat.COLUMN_BUSINESS)),
                           cursor.getString(cursor.getColumnIndex(BaseContrat.ProductsContrat.COLUMN_CATEGORY)),
                           cursor.getString(cursor.getColumnIndex(BaseContrat.ProductsContrat.COLUMN_NAME)),
                           cursor.getInt(cursor.getColumnIndex(BaseContrat.ProductsContrat.COLUMN_CARBOHYDRATE)),
                           cursor.getInt(cursor.getColumnIndex(BaseContrat.ProductsContrat.COLUMN_SLOW_SUGAR)),
                           cursor.getInt(cursor.getColumnIndex(BaseContrat.ProductsContrat.COLUMN_IMAGE))
        );
    }

    public static List<Product> getProductsFromDB(Context context, String business, String category) {
        DataBaseHelper databaseHelper = new DataBaseHelper(context);
        SQLiteDatabase db = databaseHelper.getReadableDatabase();

        String selectQuery;
        Cursor cursor;

        if(!Objects.equals(category, "")){
            selectQuery= "SELECT * FROM Products WHERE business=? AND category=?";
            cursor = db.rawQuery(selectQuery, new String[] { business, category });
        }else{
            selectQuery = "SELECT * FROM Products WHERE business=?";
            cursor = db.rawQuery(selectQuery, new String[] { business });
        }


        List<Product> listProducts = new ArrayList<>();
        if (cursor != null) {
            try {
                cursor.moveToFirst();
                while (!cursor.isAfterLast()) {
                    listProducts.add(Product.getProductFromCursor(cursor));
                    cursor.moveToNext();
                }
            }
            catch (Exception exception) {
                exception.printStackTrace();
            }
            finally {
                cursor.close();
            }
        }
        return listProducts;
    }

    public String getBusiness() {
        return business;
    }

    public String getCategory() {
        return category;
    }

    public String getName() {
        return name;
    }

    public float getSlow_sugar() {
        return (carbohydrates - fast_sugar);
    }

    public float getFast_sugar() {
        return fast_sugar;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getCarbohydrate() {
        return carbohydrates;
    }

    public void setCarbohydrates(float fast_sugar, float slow_sugar) {
        this.carbohydrates = fast_sugar + slow_sugar;
    }

    public void setFast_sugar(float fast_sugar) {
        this.fast_sugar = fast_sugar;
    }

    public int getImage() {
        return this.image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getQuantity() { return quantity; }

    public void setQuantity(int quantity) { this.quantity = quantity; }
}
