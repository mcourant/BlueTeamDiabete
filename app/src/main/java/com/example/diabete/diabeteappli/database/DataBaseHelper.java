package com.example.diabete.diabeteappli.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.diabete.diabeteappli.R;

/**
 * Created by mathilde on 03/04/2017.
 */

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String BASE_NAME = "diabete.db";
    private static final int VERSION = 10;

    public DataBaseHelper(Context context) {
        super(context, BASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(
                "CREATE TABLE Products (" +
                        BaseContrat.ProductsContrat.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        BaseContrat.ProductsContrat.COLUMN_BUSINESS + " VARCHAR(50), " +
                        BaseContrat.ProductsContrat.COLUMN_CATEGORY + " VARCHAR(100), " +
                        BaseContrat.ProductsContrat.COLUMN_NAME + " VARCHAR(250), " +
                        BaseContrat.ProductsContrat.COLUMN_CARBOHYDRATE + " FLOAT, " +
                        BaseContrat.ProductsContrat.COLUMN_SLOW_SUGAR + " FLOAT, " +
                        BaseContrat.ProductsContrat.COLUMN_IMAGE + " INTEGER);"
        );

        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Burger', 'Cheeseburger', 31, 6.9, " + R.mipmap.cheeseburger + ");" );
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Burger', 'Big Mac', 42, 8.5, " + R.mipmap.bigmac + ");" );
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Accompagnement', 'Moyenne Frite', 46, 0, " + R.mipmap.fritesmacdo + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Accompagnement', 'Moyenne Potatoes', 27, 0, " + R.mipmap.potatoesmcdo + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Boisson', 'Coca 40cl', 42, 42, " + R.mipmap.coca + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Boisson', 'Sprite 40cl', 36, 36, " + R.mipmap.sprite + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Dessert', 'McFlurry M&Ms', 31, 6.9, " + R.mipmap.mcflurry + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Dessert', 'Muffin Chocolat', 31, 6.9, " + R.mipmap.muffin + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Accompagnement', 'Ptites tomates', 2, 2, " + R.mipmap.tomates + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'McDo', 'Burger', 'Chicken McNuggets (4)', 12, 0, " + R.mipmap.chicken + ");");

        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Burger', 'Cheeseburger', 28, 7.8, " + R.mipmap.cheeseburger + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Burger', 'Big Giant', 36.4, 4.5, " + R.mipmap.bigmac + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Accompagnement', 'Moyenne Frite', 41, 0, " + R.mipmap.fritesq + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Accompagnement', 'Moyenne Rustique', 35.2, 1, " + R.mipmap.potatoesq + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Boisson', 'Coca Cola 35cl', 37.1, 37.1, " + R.mipmap.coca + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Boisson', 'Sprite 35cl', 23.1, 23.1, " + R.mipmap.sprite + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Dessert', 'Mix mania M&Ms', 58.4, 46, " + R.mipmap.mixmania + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Dessert', 'Fondant chocolat', 28, 24.1, " + R.mipmap.fondantchoco + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Accompagnement', 'Funny Tomate', 2.6, 1.6, " + R.mipmap.tomates + ");");
        db.execSQL("INSERT INTO Products VALUES (NULL, 'Quick', 'Burger', 'Chicken Dips (4)', 12.2, 0.1, " + R.mipmap.chicken + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(
                "DROP TABLE IF EXISTS Products"
        );
        onCreate(db);
    }
}
