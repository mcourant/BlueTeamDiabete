package com.example.diabete.diabeteappli.recycler_view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diabete.diabeteappli.R;

/**
 * Created by mcourant on 31/03/2017.
 */

public class viewHolderMealProposition extends RecyclerView.ViewHolder {

    private TextView textView = null;

    public TextView getTextView_sucre() {
        return textView_sucre;
    }

    private TextView textView_sucre = null;
    private RecyclerView myrecyclerview = null;

    public viewHolderMealProposition(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.proposition_title);
        textView_sucre = (TextView) itemView.findViewById(R.id.proposition_title_sucre);
        myrecyclerview = (RecyclerView) itemView.findViewById(R.id.proposition_dishes_list);
    }

    public TextView getTextView() {
        return textView;
    }

    public RecyclerView getMyrecyclerview() {
        return myrecyclerview;
    }
}
