package com.example.diabete.diabeteappli.recycler_view.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diabete.diabeteappli.R;

import org.w3c.dom.Text;

/**
 * Created by mcourant on 31/03/2017.
 */

public class viewHolderSelectMenu extends RecyclerView.ViewHolder {

    private TextView textView = null;
    private ImageView imageView = null;



    private TextView quantite = null;

    public viewHolderSelectMenu(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(R.id.textViewProduct);
        imageView = (ImageView) itemView.findViewById(R.id.imageViewProduct);
        quantite = (TextView) itemView.findViewById(R.id.textViewQuantite);
    }

    public TextView getTextView() {
        return textView;
    }

    public ImageView getImageView() {
        return imageView;
    }

    public TextView getQuantite() {
        return quantite;
    }


}
