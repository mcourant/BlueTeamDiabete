package com.example.diabete.diabeteappli.recycler_view.adapter;

import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.diabete.diabeteappli.R;
import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.recycler_view.viewholder.viewHolderSelectMenu;

import java.util.List;
import java.util.Objects;

/**
 * Created by mcourant on 31/03/2017.
 */

public class adapterSelectMenus extends RecyclerView.Adapter<viewHolderSelectMenu> {

    private List<Product> listProducts = null;

    public adapterSelectMenus(List<Product> listProducts) {
        this.listProducts = listProducts;
    }

    @Override
    public viewHolderSelectMenu onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewProduct = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product_main,parent,false);
        return new viewHolderSelectMenu(viewProduct);
    }

    @Override
    public void onBindViewHolder(viewHolderSelectMenu holder, int position) {
        holder.getTextView().setText(listProducts.get(position).getName());
        holder.getImageView().setImageResource(listProducts.get(position).getImage());
        if(listProducts.get(position).getQuantity() != 0){
            holder.getQuantite().setText("x"+listProducts.get(position).getQuantity());
        }

    }

    @Override
    public int getItemCount() {
        return listProducts.size();
    }
}
