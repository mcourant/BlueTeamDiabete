package com.example.diabete.diabeteappli;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.roundcornerprogressbar.TextRoundCornerProgressBar;
import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.mealManager.MealManager;
import com.example.diabete.diabeteappli.recycler_view.adapter.adapterSelectMenus;

import java.io.Serializable;
import org.w3c.dom.Text;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.diabete.diabeteappli.items.Product.getProductsFromDB;

public class MainActivity extends AppCompatActivity {

    public static final String PWD = "password";

    private TextRoundCornerProgressBar progressTwo;
    private RecyclerView myrecyclerview_boissons;
    private RecyclerView myrecyclerview_burger;
    private RecyclerView myrecyclerview_accompagnement;
    private RecyclerView myrecyclerview_dessert;
    private RecyclerView.Adapter myadapter_boissons;
    private RecyclerView.Adapter myadapter_burgers;
    private RecyclerView.Adapter myadapter_accompagnement;
    private RecyclerView.Adapter myadapter_dessert;
    private RecyclerView.LayoutManager mylayoutmanager;
    private RecyclerView.LayoutManager mylayoutmanager_burger;
    private RecyclerView.LayoutManager mylayoutmanager_accompagnement;
    private RecyclerView.LayoutManager mylayoutmanager_dessert;
    private GestureDetector gestureDetector;
    private FloatingActionButton fob;
    private Map<String, float[]> meal = new HashMap<>();
    private float[] protocoleMeal = new float[2];
    private MealManager mealManager = new MealManager();
    private List<Product> listProducts_boissons = new ArrayList<>();
    private List<Product> listProducts_burgers = new ArrayList<>();
    private List<Product> listProducts_accompagnement = new ArrayList<>();
    private List<Product> listProducts_dessert = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressTwo = (TextRoundCornerProgressBar) findViewById(R.id.progress_two);
        myrecyclerview_boissons = (RecyclerView) findViewById(R.id.recycler_view_menus_boissons);
        myrecyclerview_burger = (RecyclerView) findViewById(R.id.recycler_view_menus_burger);
        myrecyclerview_accompagnement = (RecyclerView) findViewById(R.id.recycler_view_menus_accompagnement);
        myrecyclerview_dessert = (RecyclerView) findViewById(R.id.recycler_view_menus_dessert);
        fob = (FloatingActionButton) findViewById(R.id.floatingActionButtonMenu);

        Intent intent = getIntent();
        final String business = intent.getStringExtra(FastFood.BUSINESS);

        progressTwo.setProgressText("x1.0");
        progressTwo.setProgress(0);
        progressTwo.setProgressBackgroundColor(getResources().getColor(R.color.darkBlue));

        myrecyclerview_boissons.setHasFixedSize(true);
        myrecyclerview_burger.setHasFixedSize(true);
        myrecyclerview_accompagnement.setHasFixedSize(true);
        myrecyclerview_dessert.setHasFixedSize(true);

        mylayoutmanager = new GridLayoutManager(this, 2);
        mylayoutmanager_burger = new GridLayoutManager(this, 2);
        mylayoutmanager_accompagnement = new GridLayoutManager(this, 2);
        mylayoutmanager_dessert = new GridLayoutManager(this, 2);
        myrecyclerview_boissons.setLayoutManager(mylayoutmanager);
        myrecyclerview_burger.setLayoutManager(mylayoutmanager_burger);
        myrecyclerview_accompagnement.setLayoutManager(mylayoutmanager_accompagnement);
        myrecyclerview_dessert.setLayoutManager(mylayoutmanager_dessert);

        listProducts_boissons = getProductsFromDB(this, business, "Boisson");
        listProducts_burgers = getProductsFromDB(this, business, "Burger");
        listProducts_accompagnement = getProductsFromDB(this, business, "Accompagnement");
        listProducts_dessert = getProductsFromDB(this, business, "Dessert");

        myadapter_boissons = new adapterSelectMenus(listProducts_boissons);
        myadapter_burgers = new adapterSelectMenus(listProducts_burgers);
        myadapter_accompagnement = new adapterSelectMenus(listProducts_accompagnement);
        myadapter_dessert = new adapterSelectMenus(listProducts_dessert);
        myrecyclerview_boissons.setAdapter(myadapter_boissons);
        myrecyclerview_burger.setAdapter(myadapter_burgers);
        myrecyclerview_accompagnement.setAdapter(myadapter_accompagnement);
        myrecyclerview_dessert.setAdapter(myadapter_dessert);

        myrecyclerview_boissons.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (gestureDetector.onTouchEvent(e))      {
                    final View child = rv.findChildViewUnder(e.getX(),e.getY());
                    if (child != null) {
                        int position = rv.getChildAdapterPosition(child);
                        final TextView txt = (TextView) child.findViewById(R.id.textViewSelected);
                        final TextView txt_quantity = (TextView) child.findViewById(R.id.textViewQuantite);
                        txt_quantity.setText("x1");
                        final String productName = listProducts_boissons.get(position).getName();

                        if (txt.getText() != "1") {
                            child.setBackgroundResource(R.drawable.background_rounded_orange);
                            txt.setText("1");
                            try {
                                mealManager.addToMeal(meal, mealManager.findProductWithName(listProducts_boissons, productName));
                                float multiplierProtocolDisplay = mealManager.multiplierProtocolCalculation(meal, protocoleMeal);
                                progressTwo.setProgressText("x" + Float.toString(multiplierProtocolDisplay));

                                updateProgressTwoColor(mealManager.getExactMultiplierProtocol());

                                progressTwo.setProgressWithAnimation(mealManager.getExactMultiplierProtocol());
                            } catch (Exception error) {
                                error.printStackTrace();
                                txt.setText("0");
                                child.setBackgroundColor(getResources().getColor(R.color.darkBlue));
                                Toast.makeText(MainActivity.this, "Erreur : L'aliment n'a pas été trouvé", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            displayQuantityPopup(listProducts_boissons.get(position), listProducts_boissons, child);
                        }

                        checkVariance();

                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        myrecyclerview_burger.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (gestureDetector.onTouchEvent(e))      {
                    final View child = rv.findChildViewUnder(e.getX(),e.getY());
                    if (child != null) {
                        int position = rv.getChildAdapterPosition(child);
                        final TextView txt = (TextView) child.findViewById(R.id.textViewSelected);
                        final TextView txt_quantity = (TextView) child.findViewById(R.id.textViewQuantite);
                        txt_quantity.setText("x1");
                        final String productName = listProducts_burgers.get(position).getName();
                        if (txt.getText() != "1") {
                            child.setBackgroundResource(R.drawable.background_rounded_orange);
                            txt.setText("1");
                            try {
                                mealManager.addToMeal(meal, mealManager.findProductWithName(listProducts_burgers, productName));
                                float multiplierProtocolDisplay = mealManager.multiplierProtocolCalculation(meal, protocoleMeal);
                                progressTwo.setProgressText("x" + Float.toString(multiplierProtocolDisplay));

                                updateProgressTwoColor(mealManager.getExactMultiplierProtocol());

                                progressTwo.setProgressWithAnimation(mealManager.getExactMultiplierProtocol());
                            } catch (Exception error) {
                                error.printStackTrace();
                                txt.setText("0");
                                child.setBackgroundColor(getResources().getColor(R.color.darkBlue));
                                Toast.makeText(MainActivity.this, "Erreur : L'aliment n'a pas été trouvé", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            displayQuantityPopup(listProducts_burgers.get(position), listProducts_burgers, child);
                        }

                        checkVariance();

                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        myrecyclerview_accompagnement.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (gestureDetector.onTouchEvent(e))      {
                    final View child = rv.findChildViewUnder(e.getX(),e.getY());
                    if (child != null) {
                        int position = rv.getChildAdapterPosition(child);
                        final TextView txt = (TextView) child.findViewById(R.id.textViewSelected);
                        final TextView txt_quantity = (TextView) child.findViewById(R.id.textViewQuantite);
                        txt_quantity.setText("x1");
                        final String productName = listProducts_accompagnement.get(position).getName();
                        if (txt.getText() != "1") {
                            child.setBackgroundResource(R.drawable.background_rounded_orange);
                            txt.setText("1");
                            try {
                                mealManager.addToMeal(meal, mealManager.findProductWithName(listProducts_accompagnement, productName));
                                float multiplierProtocolDisplay = mealManager.multiplierProtocolCalculation(meal, protocoleMeal);
                                progressTwo.setProgressText("x" + Float.toString(multiplierProtocolDisplay));

                                updateProgressTwoColor(mealManager.getExactMultiplierProtocol());

                                progressTwo.setProgressWithAnimation(mealManager.getExactMultiplierProtocol());

                                Toast.makeText(MainActivity.this, productName + " ajouté au menu !", Toast.LENGTH_SHORT).show();
                            } catch (Exception error) {
                                error.printStackTrace();
                                txt.setText("0");
                                child.setBackgroundColor(getResources().getColor(R.color.darkBlue));
                                Toast.makeText(MainActivity.this, "Erreur : L'aliment n'a pas été trouvé", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            displayQuantityPopup(listProducts_accompagnement.get(position), listProducts_accompagnement, child);
                        }

                        checkVariance();

                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        myrecyclerview_dessert.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                if (gestureDetector.onTouchEvent(e))      {
                    final View child = rv.findChildViewUnder(e.getX(),e.getY());
                    if (child != null) {
                        int position = rv.getChildAdapterPosition(child);
                        final TextView txt = (TextView) child.findViewById(R.id.textViewSelected);
                        final TextView txt_quantity = (TextView) child.findViewById(R.id.textViewQuantite);
                        txt_quantity.setText("x1");
                        final String productName = listProducts_dessert.get(position).getName();
                        if (txt.getText() != "1") {
                            child.setBackgroundResource(R.drawable.background_rounded_orange);
                            txt.setText("1");
                            try {
                                mealManager.addToMeal(meal, mealManager.findProductWithName(listProducts_dessert, productName));
                                float multiplierProtocolDisplay = mealManager.multiplierProtocolCalculation(meal, protocoleMeal);
                                progressTwo.setProgressText("x" + Float.toString(multiplierProtocolDisplay));

                                updateProgressTwoColor(mealManager.getExactMultiplierProtocol());

                                progressTwo.setProgressWithAnimation(mealManager.getExactMultiplierProtocol());

                                Toast.makeText(MainActivity.this, productName + " ajouté au menu !", Toast.LENGTH_SHORT).show();
                            } catch (Exception error) {
                                error.printStackTrace();
                                txt.setText("0");
                                child.setBackgroundColor(getResources().getColor(R.color.darkBlue));
                                Toast.makeText(MainActivity.this, "Erreur : L'aliment n'a pas été trouvé", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            displayQuantityPopup(listProducts_dessert.get(position), listProducts_dessert, child);
                        }

                        checkVariance();

                        return true;
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {}

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
        });

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override public boolean onSingleTapUp(MotionEvent event) {
                return true;
            }
        });

        fob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float variance = mealManager.getVariance();
                if(variance == 0){
                    AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.warning)
                            .setMessage(R.string.warning_protocol)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }else if(variance <= 5){
                    Intent goEatIntent = new Intent(MainActivity.this, MealActivity.class);
                    goEatIntent.putExtra("MEAL", (Serializable) meal);
                    goEatIntent.putExtra(FastFood.BUSINESS, business);
                    goEatIntent.putExtra("Protocole", protocoleMeal);
                    startActivity(goEatIntent);
                }else if(variance > 5 && variance <= 20){
                    Intent intent = new Intent(MainActivity.this, RationActivity.class);
                    intent.putExtra("business", business);
                    intent.putExtra("Menu", (Serializable) meal);
                    intent.putExtra("Protocole", protocoleMeal);
                    startActivity(intent);
                }else if(variance > 20){
                    AlertDialog alert = new AlertDialog.Builder(MainActivity.this)
                            .setTitle(R.string.warning)
                            .setMessage(R.string.warning_protocol)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                }
            }
        });


    }

    private void updateProgressTwoColor(float progress) {
        if (progress >= 0 && progress <= 1) {
            progressTwo.setProgressColor(getResources().getColor(R.color.green));
        } else if(progress > 1 && progress <= 1.5) {
            progressTwo.setProgressColor(getResources().getColor(R.color.yellow));
        } else if(progress > 1.5 && progress <= 2) {
            progressTwo.setProgressColor(getResources().getColor(R.color.salmon));
        } else if(progress > 2 && progress <= 3) {
            progressTwo.setProgressColor(getResources().getColor(R.color.red));
        } else if(progress > 3) {
            progressTwo.setProgressColor(getResources().getColor(R.color.darkRed));
        }
    }

    public void checkVariance(){
        float variance = mealManager.getVariance();
        if(variance == 0){
            fob.setImageResource(R.mipmap.less);
        }else if(variance <= 5){
            fob.setImageResource(R.mipmap.covered);
        }else if(variance > 5 && variance <= 20){
            fob.setImageResource(R.mipmap.balance);
        }else if(variance > 20){
            fob.setImageResource(R.mipmap.less);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_activity_parent, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite) {

            displayPasswordPopup(true);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setMealProtocol();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void displayQuantityPopup(Product product , final List<Product> list_selected, final View child) {
        LayoutInflater inflater = (LayoutInflater)getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup activityView = (ViewGroup)((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);

        final PopupWindow productQuantityPopup = new PopupWindow(inflater.inflate(R.layout.popup_product_quantity, null),
                (int)Math.round(activityView.getWidth()/2*1.6),
                (int)Math.round(activityView.getHeight()/2*1.6),
                true);
        productQuantityPopup.setBackgroundDrawable(new BitmapDrawable()); //dismiss the popup on outside click
        productQuantityPopup.showAtLocation(new RelativeLayout(this), Gravity.CENTER, 0, 0);

        final View popupView = productQuantityPopup.getContentView();

        final String productName = product.getName();

        // Set product image
        //TODO replace with real product picto
        popupView.findViewById(R.id.product_picto).setBackgroundResource(product.getImage());

        // Set product label
        ((TextView)popupView.findViewById(R.id.product_label)).setText(productName);

        // Set product quantity
        final int quantity = mealManager.countProduct(meal, productName);
        final NumberPicker quantityNP = (NumberPicker)popupView.findViewById(R.id.product_quantity);
        quantityNP.setMaxValue(100); // max value 100
        quantityNP.setMinValue(0);   // min value 0
        quantityNP.setValue(quantity);
        quantityNP.setWrapSelectorWheel(false);

        // OK button
        popupView.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int newProductQuantity = quantityNP.getValue();
                try {
                    int productAlreadyInside = mealManager.countProduct(meal, productName);
                    if (productAlreadyInside < newProductQuantity) {
                        for (int i = 0; i < (newProductQuantity - productAlreadyInside); i++) {
                            mealManager.addToMeal(meal, mealManager.findProductWithName(list_selected, productName));
                        }
                    } else {
                        if (productAlreadyInside < newProductQuantity) {
                            for (int i = 0; i < productAlreadyInside; i++) {
                                mealManager.removeFromMeal(meal, mealManager.findProductWithName(list_selected, productName));
                            }
                        } else if (productAlreadyInside > newProductQuantity) {
                            for (int i = 0; i < (productAlreadyInside - newProductQuantity); i++) {
                                mealManager.removeFromMeal(meal, mealManager.findProductWithName(list_selected, productName));
                            }
                        }
                    }

                    if(mealManager.countProduct(meal, productName) <= 0) {
                        final TextView txt = (TextView) child.findViewById(R.id.textViewSelected);
                        txt.setText("0");
                        child.setBackgroundColor(getResources().getColor(R.color.darkBlue));
                    }

                    String displayMultiplierProtocol = Float.toString(mealManager.multiplierProtocolCalculation(meal, protocoleMeal));
                    updateProgressTwoColor(mealManager.getExactMultiplierProtocol());
                    final TextView txt_quantity = (TextView) child.findViewById(R.id.textViewQuantite);
                    if(newProductQuantity == 0){
                        txt_quantity.setText("");
                    }else{
                        txt_quantity.setText("x"+newProductQuantity);
                    }

                    checkVariance();

                    progressTwo.setProgressText("x"+displayMultiplierProtocol);
                    progressTwo.setProgressWithAnimation(mealManager.getExactMultiplierProtocol());
                } catch (Exception e) {
                    Toast.makeText(getApplicationContext(), "Erreur : L'aliment n'a pas été trouvé", Toast.LENGTH_SHORT).show();
                }

                productQuantityPopup.dismiss();
            }
        });

        // Cancel button
        popupView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                productQuantityPopup.dismiss();
            }
        });
    }

    public void displayPasswordPopup (Boolean dismissable) {
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup activityView = (ViewGroup)((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);

        final PopupWindow passwordPopup = new PopupWindow(inflater.inflate(R.layout.popup_password, null),
                (int)Math.round(activityView.getWidth()/2*1.6),
                (int)Math.round(activityView.getHeight()/2*1.6),
                true);
        if(dismissable) {
            passwordPopup.setBackgroundDrawable(new BitmapDrawable()); //dismiss the popup on outside click
        }
        passwordPopup.showAtLocation(new RelativeLayout(this), Gravity.CENTER, 0, 0);

        final View popupView = passwordPopup.getContentView();

        // Check if pwd already set, if not change Popup text to "Choisir un mot de passe"
        final String localPwd = PreferenceManager.getDefaultSharedPreferences(this).getString(MainActivity.PWD, null);
        if(localPwd == null) {
            ((TextView)popupView.findViewById(R.id.password_label)).setText(R.string.chose_pwd);
        }

        // Popup check button functions
        popupView.findViewById(R.id.checkPwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get password entered by user, if empty display error toast
                String pwd = ((EditText)popupView.findViewById(R.id.password)).getText().toString();
                if(pwd.equals("")) {
                    Toast errorToast = Toast.makeText(popupView.getContext(), R.string.please_enter_pwd, Toast.LENGTH_LONG);
                    //errorToast.getView().setBackgroundResource(R.drawable.toast_error_background);
                    errorToast.show();
                } else if(localPwd != null && !localPwd.equals(pwd)) { // if pwd entered != saved pwd display error toast
                    Toast errorToast = Toast.makeText(popupView.getContext(), R.string.wrong_pwd, Toast.LENGTH_LONG);
                    //errorToast.getView().setBackgroundResource(R.drawable.toast_error_background);
                    errorToast.show();
                } else if(localPwd == null) { // if pwd doesn't exist, check saves the password entered and redirects to settings
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(MainActivity.this).edit();
                    editor.putString(MainActivity.PWD, pwd);
                    editor.apply();

                    Toast.makeText(popupView.getContext(), R.string.pwd_saved, Toast.LENGTH_LONG).show();

                    passwordPopup.dismiss();

                    Intent intent = new Intent(MainActivity.this, ProtocolSettingsActivity.class);
                    startActivity(intent);
                } else if (localPwd != null && localPwd.equals(pwd)) { // if saved pwd exists and matches entered pwd, redirects to settings
                    passwordPopup.dismiss();

                    Intent intent = new Intent(MainActivity.this, ProtocolSettingsActivity.class);
                    startActivity(intent);
                }
            }
        });

        Button cancel = (Button)popupView.findViewById(R.id.cancel);
        if(dismissable) {
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    passwordPopup.dismiss();
                }
            });
        } else {
            cancel.setVisibility(View.INVISIBLE);
        }
    }

    private void setMealProtocol() {
        int selectedMeal = this.getIntent().getIntExtra(FastFood.MEAL, -1);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        float breakfastFast = preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_FAST, 0f);
        float breakfastSlow = preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_SLOW, 0f);
        float lunchFast = preferences.getFloat(ProtocolSettingsActivity.LUNCH_FAST, 0f);
        float lunchSlow = preferences.getFloat(ProtocolSettingsActivity.LUNCH_SLOW, 0f);
        float snackFast = preferences.getFloat(ProtocolSettingsActivity.SNACK_FAST, 0f);
        float snackSlow = preferences.getFloat(ProtocolSettingsActivity.SNACK_SLOW, 0f);
        float dinnerFast = preferences.getFloat(ProtocolSettingsActivity.DINNER_FAST, 0f);
        float dinnerSlow = preferences.getFloat(ProtocolSettingsActivity.DINNER_SLOW, 0f);

        if (breakfastFast == 0 || breakfastSlow == 0 || lunchFast == 0 || lunchSlow == 0
                || snackFast == 0 || snackSlow == 0 || dinnerFast == 0 || dinnerSlow == 0) {
            AlertDialog.Builder noProtocoleAlert = new AlertDialog.Builder(this);
            noProtocoleAlert.setTitle(R.string.no_protocol_title);
            noProtocoleAlert.setMessage(R.string.no_protocol_msg);
            noProtocoleAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    displayPasswordPopup(false);
                }
            });
            noProtocoleAlert.show();
        } else {
            switch (selectedMeal) {
                case 0:
                    protocoleMeal[0] = preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_FAST, 1f);
                    protocoleMeal[1] = preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_SLOW, 1f);
                    break;
                case 1:
                    protocoleMeal[0] = preferences.getFloat(ProtocolSettingsActivity.LUNCH_FAST, 1f);
                    protocoleMeal[1] = preferences.getFloat(ProtocolSettingsActivity.LUNCH_SLOW, 1f);
                    break;
                case 2:
                    protocoleMeal[0] = preferences.getFloat(ProtocolSettingsActivity.SNACK_FAST, 1f);
                    protocoleMeal[1] = preferences.getFloat(ProtocolSettingsActivity.SNACK_SLOW, 1f);
                    break;
                case 3:
                    protocoleMeal[0] = preferences.getFloat(ProtocolSettingsActivity.DINNER_FAST, 1f);
                    protocoleMeal[1] = preferences.getFloat(ProtocolSettingsActivity.DINNER_SLOW, 1f);
                    break;
                default:
                    break;
            }
        }
    }

}
