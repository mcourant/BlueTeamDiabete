package com.example.diabete.diabeteappli;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class ProtocolSettingsActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String BREAKFAST_FAST = "breakfast_fast";
    public static final String BREAKFAST_SLOW = "breakfast_slow";
    public static final String LUNCH_FAST = "lunch_fast";
    public static final String LUNCH_SLOW = "lunch_slow";
    public static final String SNACK_FAST = "snack_fast";
    public static final String SNACK_SLOW = "snack_slow";
    public static final String DINNER_FAST = "dinner_fast";
    public static final String DINNER_SLOW = "dinner_slow";

    private EditText breakfastFast;
    private EditText breakfastSlow;
    private EditText lunchFast;
    private EditText lunchSlow;
    private EditText snackFast;
    private EditText snackSlow;
    private EditText dinnerFast;
    private EditText dinnerSlow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_protocol_settings);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        this.breakfastFast = (EditText)this.findViewById(R.id.breakfast_fast);
        this.breakfastSlow = (EditText)this.findViewById(R.id.breakfast_slow);
        this.lunchFast = (EditText)this.findViewById(R.id.lunch_fast);
        this.lunchSlow = (EditText)this.findViewById(R.id.lunch_slow);
        this.snackFast = (EditText)this.findViewById(R.id.snack_fast);
        this.snackSlow = (EditText)this.findViewById(R.id.snack_slow);
        this.dinnerFast = (EditText)this.findViewById(R.id.dinner_fast);
        this.dinnerSlow = (EditText)this.findViewById(R.id.dinner_slow);

        this.breakfastFast.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_FAST, 0f)));
        this.breakfastSlow.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.BREAKFAST_SLOW, 0f)));
        this.lunchFast.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.LUNCH_FAST, 0f)));
        this.lunchSlow.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.LUNCH_SLOW, 0f)));
        this.snackFast.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.SNACK_FAST, 0f)));
        this.snackSlow.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.SNACK_SLOW, 0f)));
        this.dinnerFast.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.DINNER_FAST, 0f)));
        this.dinnerSlow.setText(String.valueOf(preferences.getFloat(ProtocolSettingsActivity.DINNER_SLOW, 0f)));

        this.findViewById(R.id.cancel).setOnClickListener(this);
        this.findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.save:
                float breakfastFast = Float.valueOf(ProtocolSettingsActivity.this.breakfastFast.getText().toString());
                float breakfastSlow = Float.valueOf(ProtocolSettingsActivity.this.breakfastSlow.getText().toString());
                float lunchFast = Float.valueOf(ProtocolSettingsActivity.this.lunchFast.getText().toString());
                float lunchSlow = Float.valueOf(ProtocolSettingsActivity.this.lunchSlow.getText().toString());
                float snackFast = Float.valueOf(ProtocolSettingsActivity.this.snackFast.getText().toString());
                float snackSlow = Float.valueOf(ProtocolSettingsActivity.this.snackSlow.getText().toString());
                float dinnerFast = Float.valueOf(ProtocolSettingsActivity.this.dinnerFast.getText().toString());
                float dinnerSlow = Float.valueOf(ProtocolSettingsActivity.this.dinnerSlow.getText().toString());

                if (breakfastFast == 0 || breakfastSlow == 0 || lunchFast == 0 || lunchSlow == 0
                        || snackFast == 0 || snackSlow == 0 || dinnerFast == 0 || dinnerSlow == 0) {
                    AlertDialog.Builder missingFieldsAlert = new AlertDialog.Builder(this);
                    missingFieldsAlert.setTitle(R.string.missing_fields_title);
                    missingFieldsAlert.setMessage(R.string.missing_fields_msg);
                    missingFieldsAlert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    missingFieldsAlert.show();
                } else {
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ProtocolSettingsActivity.this).edit();

                    editor.putFloat(ProtocolSettingsActivity.BREAKFAST_FAST, breakfastFast);
                    editor.putFloat(ProtocolSettingsActivity.BREAKFAST_SLOW, breakfastSlow);
                    editor.putFloat(ProtocolSettingsActivity.LUNCH_FAST, lunchFast);
                    editor.putFloat(ProtocolSettingsActivity.LUNCH_SLOW, lunchSlow);
                    editor.putFloat(ProtocolSettingsActivity.SNACK_FAST, snackFast);
                    editor.putFloat(ProtocolSettingsActivity.SNACK_SLOW, snackSlow);
                    editor.putFloat(ProtocolSettingsActivity.DINNER_FAST, dinnerFast);
                    editor.putFloat(ProtocolSettingsActivity.DINNER_SLOW, dinnerSlow);

                    editor.apply();

                    Toast.makeText(ProtocolSettingsActivity.this, R.string.protocols_saved, Toast.LENGTH_SHORT).show();

                    finish();
                }
                break;

            case R.id.cancel:
                finish();
                break;
            default: break;
        }
    }

    public void displayPasswordPopup () {
        LayoutInflater inflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup activityView = (ViewGroup)((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);

        final PopupWindow passwordPopup = new PopupWindow(inflater.inflate(R.layout.popup_password, null),
                (int)Math.round(activityView.getWidth()/2*1.6),
                (int)Math.round(activityView.getHeight()/2*1.6),
                true);
        passwordPopup.setBackgroundDrawable(new BitmapDrawable()); //dismiss the popup on outside click
        passwordPopup.showAtLocation(new RelativeLayout(this), Gravity.CENTER, 0, 0);

        final View popupView = passwordPopup.getContentView();

        // Check if pwd already set, if not change Popup text to "Choisir un mot de passe"
        final String localPwd = PreferenceManager.getDefaultSharedPreferences(this).getString(MainActivity.PWD, null);
        if(localPwd == null) {
            ((TextView)popupView.findViewById(R.id.password_label)).setText(R.string.chose_pwd);
        }

        // Popup check button functions
        popupView.findViewById(R.id.checkPwd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get password entered by user, if empty display error toast
                String pwd = ((EditText)popupView.findViewById(R.id.password)).getText().toString();
                if(pwd.equals("")) {
                    Toast errorToast = Toast.makeText(popupView.getContext(), R.string.please_enter_pwd, Toast.LENGTH_LONG);
                    //errorToast.getView().setBackgroundResource(R.drawable.toast_error_background);
                    errorToast.show();
                } else if(localPwd != null && !localPwd.equals(pwd)) { // if pwd entered != saved pwd display error toast
                    Toast errorToast = Toast.makeText(popupView.getContext(), R.string.wrong_pwd, Toast.LENGTH_LONG);
                    //errorToast.getView().setBackgroundResource(R.drawable.toast_error_background);
                    errorToast.show();
                } else if(localPwd == null) { // if pwd doesn't exist, check saves the password entered and redirects to settings
                    SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(ProtocolSettingsActivity.this).edit();
                    editor.putString(MainActivity.PWD, pwd);
                    editor.apply();

                    Toast.makeText(popupView.getContext(), R.string.pwd_saved, Toast.LENGTH_LONG).show();

                    passwordPopup.dismiss();

                    /*Intent intent = new Intent(MainActivity.this, ProtocolSettingsActivity.class);
                    startActivity(intent);*/
                } else if (localPwd != null && localPwd.equals(pwd)) { // if saved pwd exists and matches entered pwd, redirects to settings
                    passwordPopup.dismiss();

                    /*Intent intent = new Intent(MainActivity.this, ProtocolSettingsActivity.class);
                    startActivity(intent);*/
                }
            }
        });

        popupView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                passwordPopup.dismiss();
            }
        });
    }
}
