package com.example.diabete.diabeteappli;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.mealManager.MealManager;
import com.example.diabete.diabeteappli.recycler_view.adapter.adapterSelectMenus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.diabete.diabeteappli.items.Product.getProductsFromDB;

public class RationActivity extends AppCompatActivity implements RecyclerView.OnItemTouchListener {

    private MealManager mealManager = new MealManager();
    private Map<String, float[]> meal = new HashMap<>();
    private List<Product> listProductSelected = new ArrayList<>();
    private GestureDetector gestureDetector;
    private String business;
    private float[] protocolMeal = new float[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ration);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        RecyclerView myrecyclerview = (RecyclerView) findViewById(R.id.recycler_view_menu_for_selected);

        FloatingActionButton fob = (FloatingActionButton) findViewById(R.id.floatingActionButtonMenuRation);

        myrecyclerview.setHasFixedSize(true);

        Intent intent = getIntent();
        meal = (Map<String, float[]>) intent.getSerializableExtra("Menu");
        business = intent.getStringExtra("business");
        protocolMeal = intent.getFloatArrayExtra("Protocole");

        List<Product>listProduct = getProductsFromDB(this, business, "");


        for(String productName : meal.keySet()) {
            String[] productname = productName.split("_");
            for( int i = 0; i < listProduct.size(); i++){
                if(listProduct.get(i).getName().equals(productname[0])){
                    listProductSelected.add(listProduct.get(i));
                }
            }
        }

        RecyclerView.LayoutManager mylayoutmanager = new GridLayoutManager(this,2);
        myrecyclerview.setLayoutManager(mylayoutmanager);

        RecyclerView.Adapter myadapter = new adapterSelectMenus(listProductSelected);
        myrecyclerview.setAdapter(myadapter);

        myrecyclerview.addOnItemTouchListener(this);

        gestureDetector = new GestureDetector(this, new GestureDetector.SimpleOnGestureListener() {
            @Override public boolean onSingleTapUp(MotionEvent event) {
                return true;
            }
        });

        fob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RationActivity.this,EatActivity.class);
                intent.putExtra("MEALRATION", (Serializable) meal);
                intent.putExtra(FastFood.BUSINESS, business);
                intent.putExtra("Protocole", protocolMeal);
                startActivity(intent);
            }
        });

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

        if (gestureDetector.onTouchEvent(e))      {
            final View child = rv.findChildViewUnder(e.getX(),e.getY());
            if (child != null) {
                int position = rv.getChildAdapterPosition(child);

                    TextView textView = (TextView) child.findViewById(R.id.textViewSelected);
                    if (textView.getText() != "1") {
                        if(mealManager.getmealCanBeRationed(meal)) {
                            textView.setText("1");
                            child.setBackgroundResource(R.drawable.background_rounded_orange);
                            mealManager.lockProduct(meal, listProductSelected.get(position), true);
                        }else{
                            Toast.makeText(this, R.string.cannot_add_any_more_product, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        textView.setText("0");
                        child.setBackgroundResource(R.drawable.background_rounded_dark_blue);
                        mealManager.lockProduct(meal, listProductSelected.get(position), false);
                    }
                    return true;

            }
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
