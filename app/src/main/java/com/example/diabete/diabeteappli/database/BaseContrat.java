package com.example.diabete.diabeteappli.database;

import android.provider.BaseColumns;

/**
 * Created by mathilde on 03/04/2017.
 */

public final class BaseContrat {
        // constructeur privé afin de ne pas instancier la classe :
        private BaseContrat() {}

        // contenu de la table "products" :
        public static class ProductsContrat implements BaseColumns {
            public static final String TABLE_PRODUCTS = "products";
            public static final String COLUMN_ID = "id";
            public static final String COLUMN_BUSINESS = "business";
            public static final String COLUMN_CATEGORY = "category";
            public static final String COLUMN_NAME = "name";
            public static final String COLUMN_CARBOHYDRATE = "carbohydrate";
            public static final String COLUMN_SLOW_SUGAR = "slow_sugar";
            public static final String COLUMN_IMAGE = "image_name";
        }
}
