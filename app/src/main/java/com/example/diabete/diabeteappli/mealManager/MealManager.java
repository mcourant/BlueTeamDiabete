package com.example.diabete.diabeteappli.mealManager;

import android.os.Parcel;
import android.os.Parcelable;

import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.items.Proposition;

import java.util.*;

public class MealManager implements Parcelable {
    private float variance;
    private boolean isVarianceGretterThanProtocol;
    private float MultiplierProtocol;
    private float exactMultiplierProtocol;

    public MealManager() {}

    protected MealManager(Parcel in) {
        variance = in.readFloat();
        isVarianceGretterThanProtocol = in.readByte() != 0;
        MultiplierProtocol = in.readFloat();
        exactMultiplierProtocol = in.readFloat();
    }

    public static final Creator<MealManager> CREATOR = new Creator<MealManager>() {
        @Override
        public MealManager createFromParcel(Parcel in) {
            return new MealManager(in);
        }

        @Override
        public MealManager[] newArray(int size) {
            return new MealManager[size];
        }
    };

    public float getVariance() {
        return variance;
    }

    public float getExactMultiplierProtocol() {
        return exactMultiplierProtocol;
    }

    public float multiplierProtocolCalculation(Map<String, float[]> meal, float[] protocolMeal) throws Exception {
        float[] MultiplierSugars = {0 /* rapides */, 0 /* lents */};
        float sugars[] = {0 /* rapides */, 0 /* lents */};

        for(String name : meal.keySet()) {
            float[] data = meal.get(name);
            sugars[0] += data[1] * (data[3] / 100);
            sugars[1] += (data[0] - data[1]) * (data[3] / 100);
        }

        MultiplierSugars[0] = sugars[0] / protocolMeal[0];
        MultiplierSugars[1] = sugars[1] / protocolMeal[1];
        //exactMultiplierProtocol contains the multiplier with an accuracy of one decimal
        this.exactMultiplierProtocol = (MultiplierSugars[0] + MultiplierSugars[1]) / 2;

        this.variance = Math.abs(sugars[0] - protocolMeal[0] * (float) (Math.floor((double) (MultiplierSugars[0])))) + Math.abs(sugars[1] - protocolMeal[1] * (float) (Math.floor((double)(MultiplierSugars[1]))));
        float relativeVariance = (sugars[0] - protocolMeal[0] * Math.round(MultiplierSugars[0])) + (sugars[1] - protocolMeal[1] * Math.round(MultiplierSugars[1]));

        if (relativeVariance > 0 && relativeVariance > 5) {
            isVarianceGretterThanProtocol = true;
        } else {
            isVarianceGretterThanProtocol = false;
        }

        if (MultiplierSugars[0] >= 1.25 && MultiplierSugars[0] <= 1.75) {
            MultiplierSugars[0] = 1.5f;
        } else {
            MultiplierSugars[0] = Math.round(MultiplierSugars[0]);
        }

        if (MultiplierSugars[1] >= 1.25 && MultiplierSugars[1] <= 1.75) {
            MultiplierSugars[1] = 1.5f;
        } else {
            MultiplierSugars[1] = Math.round(MultiplierSugars[1]);
        }

        this.MultiplierProtocol = (MultiplierSugars[0] + MultiplierSugars[1]) / 2;

        if (this.MultiplierProtocol >= 1.25 && this.MultiplierProtocol <= 1.75) {
            this.MultiplierProtocol = 1.5f;
        } else {
            this.MultiplierProtocol = Math.round(this.MultiplierProtocol);
            if(this.MultiplierProtocol == 0) {
                this.MultiplierProtocol = 1;
            }
        }

        if(this.MultiplierProtocol <= 0)  {
            throw new Exception("Erreur : facteur protocole final");
        } else {
            return this.MultiplierProtocol;
        }
    }

    //Calculating all the combination with all foods and create protocols
    public List<String[]> productRatio(Map<String, float[]> meal, float[] protocolMeal) throws Exception {
        List<String> ProductToRationNames = new ArrayList<>();

        if(this.MultiplierProtocol <= 0) {
            throw new Exception("Erreur : facteur protocole non calculé.");
        }

        try {
            for (String name : meal.keySet()) {
                float[] data = meal.get(name);
                if (data[2] == 0) {
                    ProductToRationNames.add(name);
                }
            }

            int[] ratios = {25, 50, 75, 100};
            String[] input = new String[ProductToRationNames.size() * ratios.length];

            int c = 0;
            for (String name : ProductToRationNames) {
                for (int i = c * ratios.length; i < ratios.length + c * ratios.length; i++) {
                    input[i] = name + ";" + ratios[i % ratios.length];
                }
                c++;
            }

            int k = input.length / ratios.length;
            //Proposition proposition = new Proposition();
            List<String[]> subsets = new ArrayList<>();
            int[] s = new int[k];

            if (k <= input.length) {
                for (int i = 0; (s[i] = i) < k - 1; i++) ;
                subsets.add(getSubset(input, s));
                for (; ; ) {
                    int i;
                    for (i = k - 1; i >= 0 && s[i] == input.length - k + i; i--) ;
                    if (i < 0) {
                        break;
                    } else {
                        s[i]++;
                        for (++i; i < k; i++) {
                            s[i] = s[i - 1] + 1;
                        }
                        subsets.add(getSubset(input, s));
                    }
                }
            }

            for (String[] proposition : new ArrayList<>(subsets)) {
                List<String> allProductNamesInProposition = new ArrayList<>();
                for (String eachProductName : proposition) {
                    allProductNamesInProposition.add(eachProductName.split(";")[0]);
                }

                for (String product : proposition) {
                    String productName = product.split(";")[0];
                    int numberOfSameProduct = 0;
                    for (String otherProductName : allProductNamesInProposition) {
                        if (otherProductName.contains(productName)) {
                            numberOfSameProduct++;
                        }
                    }

                    if (numberOfSameProduct > 1) {
                        subsets.remove(proposition);
                        break;
                    }
                }
            }

            //Here, we have all the possible combinations.
            List<String[]> propositionsWithASugarCube = new ArrayList<>();
            int id = 0;
            for (String[] proposition : new ArrayList<>(subsets)) {
                id++;
                for (String eachProductName : proposition) {
                    setProductRationInMeal(meal, eachProductName.split(";")[0], Float.parseFloat(eachProductName.split(";")[1]));
                }

                float displayMultiplier = multiplierProtocolCalculation(meal, protocolMeal);

                String[] propositionWithDatas = new String[proposition.length + 3];
                propositionWithDatas[0] = "id_" + id;
                propositionWithDatas[1] = Float.toString(displayMultiplier);
                propositionWithDatas[2] = "0";
                for(int i = 0; i < proposition.length; i++) {
                    propositionWithDatas[i+3] = proposition[i];
                }

                if (getVariance() > 5) {
                 /*   if (getVariance() <= 10 && !isVarianceGretterThanProtocol) {
                        propositionWithDatas[1] = "1";
                        propositionsWithASugarCube.add(propositionWithDatas);
                    }*/
                    subsets.remove(proposition);
                } else {
                    subsets.set(subsets.indexOf(proposition), propositionWithDatas);
                }
            }

            List<String[]> tenSubsets = new ArrayList<>();
            if(subsets.size() > 10) {
                for(int i = 0; i < 10; i++) {
                    tenSubsets.add(subsets.get(i));
                }
            } else {
                tenSubsets.addAll(subsets);
            }
            return tenSubsets;
        } catch (Exception e) {
            throw new Exception("Erreur : rationnement non calculé");
        }
    }

    private String[] getSubset(String[] input, int[] subset) {
        String[] result = new String[subset.length];
        for (int i = 0; i < subset.length; i++)
            result[i] = input[subset[i]];
        return result;
    }

    public void addToMeal(Map<String, float[]> meal, Product product) {
        int numberOfSameProduct = countProduct(meal, product);

        float[] values = {product.getCarbohydrate(), product.getFast_sugar(), 0, 100};
        meal.put(product.getName() + "_" + (numberOfSameProduct + 1), values);
    }

    public void removeFromMeal(Map<String, float[]> meal, List<Product> allProducts, String productName) throws Exception {
        Product product = findProductWithName(allProducts, productName);
        removeFromMeal(meal, product);
    }

    public void removeFromMeal(Map<String, float[]> meal, Product product) {
        int numberOfSameProduct = 0;
        for (String name : meal.keySet()) {
            if(name.contains(product.getName())) {
                numberOfSameProduct++;
            }
        }

        for (String productName : new ArrayList<>(meal.keySet())) {
            if(productName.equals(product.getName() + "_" + numberOfSameProduct)) {
                meal.remove(productName);
                break;
            }
        }
    }

    public Product findProductWithName(List<Product> allProducts, String name) throws Exception {
        for(Product product : allProducts) {
            if(product.getName().equals(name)) {
                return product;
            }
        }

        throw new Exception("Erreur : not found");
    }

    private void setProductRationInMeal(Map<String, float[]> meal, String product, float ratio) {
        for(String productName : meal.keySet()) {
            if (productName.equals(product)) {
                meal.get(productName)[3] = ratio;
            }
        }
    }

    public void lockProduct(Map<String, float[]> meal, Product product, boolean isNowLocked) {
        for(String productName : meal.keySet()) {
            if (productName.contains(product.getName())) {
                if (isNowLocked) {
                    meal.get(productName)[2] = 1;
                } else {
                    meal.get(productName)[2] = 0;
                }
            }
        }
    }

    public void lockProduct(Map<String, float[]> meal, List<Product> allProducts, String productName, boolean isNowLocked) throws Exception {
        Product product = findProductWithName(allProducts, productName);
        lockProduct(meal, product, isNowLocked);
    }

    public void selectProposition(Map<String, float[]> meal, List<String[]> subsets, int propositionId) {
        for (String[] proposition : subsets) {
            if (propositionId == Integer.parseInt(proposition[0].split("_")[1])) {
                for (int i = 3; i < proposition.length; i++) {
                    setProductRationInMeal(meal, proposition[i].split(";")[0], Float.parseFloat(proposition[i].split(";")[1]));
                }
            }
        }
    }

    //Selecting an element we absolutely want to eat
    public void selectProposition(Map<String, float[]> meal, List<String[]> subsets, String propositionId) {
        selectProposition(meal, subsets, Integer.parseInt(propositionId.split("_")[1]));
    }

    public int countProduct(Map<String, float[]> meal, Product product) {
        int numberOfSameProduct = 0;

        for (String name : meal.keySet()) {
            if(name.contains(product.getName())) {
                numberOfSameProduct++;
            }
        }

        return numberOfSameProduct;
    }

    public int countProduct(Map<String, float[]> meal, String productName) {
        int numberOfSameProduct = 0;

        for (String name : meal.keySet()) {
            if(name.contains(productName)) {
                numberOfSameProduct++;
            }
        }

        return numberOfSameProduct;
    }

    public float[] getMealCarboHydrates(Map<String, float[]> meal) {
        float mealCarboHydrates[] = {0 /* rapides */, 0 /* lents */};
        for (String name : meal.keySet()) {
            float[] data = meal.get(name);
            mealCarboHydrates[0] += data[1] * (data[3] / 100);
            mealCarboHydrates[1] += (data[0] - data[1]) * (data[3] / 100);
        }
        return mealCarboHydrates;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeFloat(variance);
        dest.writeByte((byte) (isVarianceGretterThanProtocol ? 1 : 0));
        dest.writeFloat(MultiplierProtocol);
        dest.writeFloat(exactMultiplierProtocol);
    }

    public boolean getmealCanBeRationed(Map<String, float[]> meal) {
        boolean canBeRationed = true;
        float nbrOfLockedProduct = 0;
        for (String productName : meal.keySet()) {
            float[] data = meal.get(productName);
            if(data[2] == 1) {
                nbrOfLockedProduct++;
            }
        }
        if(meal.size() < 3) {
            canBeRationed = false;
        } else if(meal.size() == 3) {
            if (nbrOfLockedProduct >= 1) {
                canBeRationed = false;
            }
        } else if (meal.size() > 3) {
            if (nbrOfLockedProduct >= (meal.size() - 2)) {
                canBeRationed = false;
            }
        }
        return canBeRationed;
    }
}
