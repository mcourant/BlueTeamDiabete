package com.example.diabete.diabeteappli.recycler_view.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.diabete.diabeteappli.R;
import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.mealManager.MealManager;
import com.example.diabete.diabeteappli.recycler_view.viewholder.viewHolderMealProposition;
import com.example.diabete.diabeteappli.recycler_view.viewholder.viewHolderSelectMenu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mcourant on 31/03/2017.
 */

public class adapterPropositionMeal extends RecyclerView.Adapter<viewHolderMealProposition> {

    private Map<String, float[]> meal = new HashMap<>();
    private List<String[]> list_propositions = new ArrayList<>();
    private Context context;
    private String business;

    public adapterPropositionMeal(List<String[]> list_propositions, Context context, Map<String, float[]> meal, String business ) {
        this.meal = meal;
        this.list_propositions = list_propositions;
        this.context = context;
        this.business = business;
    }

    @Override
    public viewHolderMealProposition onCreateViewHolder(ViewGroup parent, int viewType) {
        View viewProduct = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_proposition_card,parent,false);
        return new viewHolderMealProposition(viewProduct);
    }

    @Override
    public void onBindViewHolder(viewHolderMealProposition holder, int position) {

        List<String> list_product_string = new ArrayList<>();
        int position_deux = position+1;
        holder.getTextView().setText("Proposition "+position_deux+ " - x"+list_propositions.get(position)[1]);
        MealManager mealManager = new MealManager();

        mealManager.selectProposition(this.meal, this.list_propositions, this.list_propositions.get(position)[0]);
        float fast_sugar = (int) mealManager.getMealCarboHydrates(this.meal)[0];
        float last_sugar = (int) mealManager.getMealCarboHydrates(this.meal)[1];

        int fast_sugar_int = (int) fast_sugar;
        int last_sugar_int = (int) last_sugar;

        holder.getTextView_sucre().setText("SR : "+fast_sugar_int+"g / SL : "+last_sugar_int+"g");

        RecyclerView myrecyclerview = holder.getMyrecyclerview();
        myrecyclerview.setHasFixedSize(true);

        RecyclerView.LayoutManager mylayoutmanager = new LinearLayoutManager(this.context);
        myrecyclerview.setLayoutManager(mylayoutmanager);

        RecyclerView.Adapter myadapter = new adapterPropositionList(this.meal,business, context);
        myrecyclerview.setAdapter(myadapter);


        System.out.println(list_product_string);


    }

    @Override
    public int getItemCount() {
        return this.list_propositions.size();
    }
}
