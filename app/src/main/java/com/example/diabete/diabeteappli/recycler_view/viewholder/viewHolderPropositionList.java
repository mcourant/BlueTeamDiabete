package com.example.diabete.diabeteappli.recycler_view.viewholder;

import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.diabete.diabeteappli.R;

/**
 * Created by mcourant on 31/03/2017.
 */

public class viewHolderPropositionList extends RecyclerView.ViewHolder {

    private TextView textviewFirst = null;
    private TextView textviewSecond = null;
    private ImageView imageView = null;

    public viewHolderPropositionList(View itemView) {
        super(itemView);
        textviewFirst = (TextView) itemView.findViewById(R.id.dish_label);
        textviewSecond = (TextView) itemView.findViewById(R.id.dish_proportion);
        imageView= (ImageView) itemView.findViewById(R.id.dish_icon);
    }

    public TextView getTextviewFirst() {
        return textviewFirst;
    }

    public TextView getTextviewSecond() {
        return textviewSecond;
    }

    public ImageView getImageView() {
        return imageView;
    }
}
