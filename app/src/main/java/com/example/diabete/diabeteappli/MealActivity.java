package com.example.diabete.diabeteappli;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.mealManager.MealManager;
import com.example.diabete.diabeteappli.recycler_view.adapter.adapterSelectMenus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.diabete.diabeteappli.items.Product.getProductsFromDB;

public class MealActivity extends AppCompatActivity {

    private Map<String, float[]> meal = new HashMap<>();
    private List<Product> listProduct = new ArrayList<>();
    private List<Product> listProductSelected = new ArrayList<>();
    private String business;

    private RecyclerView myrecyclerview;
    private RecyclerView.Adapter myadapter;
    private RecyclerView.LayoutManager mylayoutmanager;
    private float[] protocolMeal = new float[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        myrecyclerview = (RecyclerView) findViewById(R.id.recycler_view_products);
        myrecyclerview.setHasFixedSize(true);

        Intent intent = getIntent();
        this.meal = (Map<String, float[]>) intent.getSerializableExtra("MEAL");
        business = intent.getStringExtra("business");
        protocolMeal = intent.getFloatArrayExtra("Protocole");

        listProduct = getProductsFromDB(this, business, "");

        for(String productName : meal.keySet()) {
            String[] productname = productName.split("_");
            for( int i = 0; i < listProduct.size(); i++){
                if(listProduct.get(i).getName().equals(productname[0])){
                    listProductSelected.add(listProduct.get(i));
                }
            }
        }

        mylayoutmanager = new GridLayoutManager(this,2);
        myrecyclerview.setLayoutManager(mylayoutmanager);

        myadapter = new adapterSelectMenus(listProductSelected);
        myrecyclerview.setAdapter(myadapter);

        MealManager mealManager = new MealManager();
        float[] carbohydrates = mealManager.getMealCarboHydrates(this.meal);

        ((TextView)this.findViewById(R.id.sr_label)).setText(String.format(this.getString(R.string.fast_sugar), (int)carbohydrates[0]));
        ((TextView)this.findViewById(R.id.sl_label)).setText(String.format(this.getString(R.string.slow_sugar), (int)carbohydrates[1]));

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
