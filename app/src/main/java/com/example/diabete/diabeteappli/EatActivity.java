package com.example.diabete.diabeteappli;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.diabete.diabeteappli.items.Product;
import com.example.diabete.diabeteappli.mealManager.MealManager;
import com.example.diabete.diabeteappli.recycler_view.adapter.adapterPropositionMeal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EatActivity extends AppCompatActivity {

    private Map<String, float[]> meal = new HashMap<>();
    private List<String[]> listPropositions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eat);
        Toolbar toolbar = (Toolbar) findViewById(R.id.eat_toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        Intent intent = this.getIntent();
        float[] protocolMeal = intent.getFloatArrayExtra("Protocole");
        if(intent.getSerializableExtra("MEAL") != null) { // meal comes from je mange
            this.meal = (Map<String, float[]>)intent.getSerializableExtra("MEAL");
        } else if(intent.getSerializableExtra("MEALRATION") != null) { // meal comes from ration
            this.meal = (Map<String, float[]>)intent.getSerializableExtra("MEALRATION");
            MealManager mealManager = new MealManager();
            try {
                mealManager.multiplierProtocolCalculation(meal, protocolMeal);
                this.listPropositions = mealManager.productRatio(this.meal, protocolMeal);
            } catch (Exception e) {
                System.out.println(e);
            }
        }

        String business = intent.getStringExtra(FastFood.BUSINESS);

        RecyclerView mealPropositionsList = (RecyclerView)findViewById(R.id.meal_propositions_list);

        if(this.listPropositions.size() ==  0) {
            this.findViewById(R.id.textforlegend).setVisibility(View.GONE);
            mealPropositionsList.setVisibility(View.GONE);
            this.findViewById(R.id.sorry_layout).setVisibility(View.VISIBLE);

            this.findViewById(R.id.back_to_product_choice).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });

        } else {
            mealPropositionsList.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            mealPropositionsList.setLayoutManager(layoutManager);

            adapterPropositionMeal mealPropositionsListAdapter;
            mealPropositionsListAdapter = new adapterPropositionMeal(this.listPropositions, getApplicationContext(), this.meal, business);
            mealPropositionsList.setAdapter(mealPropositionsListAdapter);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
